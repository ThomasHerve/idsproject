package Launcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import Node.Node;
import Node.NodeManager;
import Packet.PacketServer;
import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

public class Launcher {
	 public static String CONNECTION_NAME = "my_app";
	 public static String CONNECTION_SENDER_NAME = "my_app_sender";
	 static Random random = new Random();
	 static Channel exchangeConnection;
	 
	 
	 static HashMap<String,String> PlayerFilter = new HashMap<String,String>();

	/**
	 * Fonction d'entrée coté serveur, argument : aucun pour se connecter à un serveur RabbitMQ local, "cloud" pour se connecter a un serveur distant
	 *
	 */
	 public static void main(String[] argv) throws Exception {
	 	    String uri;
			if(argv.length > 0 && argv[0].equals("cloud")){
				uri = "amqp://sdwugoov:XPyB55TBXi7XU8yOYE2U9G8GRV4IXy50@chinook.rmq.cloudamqp.com/sdwugoov";
				System.out.println("Tentative de connection à un serveur RabbitMQ distant");
			} else {
				uri = "amqp://guest:guest@localhost";
				System.out.println("Tentative de connection à un serveur RabbitMQ local");
			}

			ConnectionFactory factory = new ConnectionFactory();
			factory.setUri(uri);

		 	Connection connection;
			try{
				connection = factory.newConnection();
			} catch (Exception e){
				if(argv.length > 0 && argv[0].equals("cloud")){
					System.out.println("Connection au serveur distant impossible, êtes vous connectés a internet ? le serveur est peut-être indisponible");
				} else {
					System.out.println("Connection au serveur local impossible, RabbitMQ ne tourne pas sur cette machine");
				}
				return;
			}
			System.out.println("Connecté !");
	        
	        //queue simple de reception de demande de connection
	        Channel connectionRequest = connection.createChannel();
	        connectionRequest.queueDeclare(CONNECTION_NAME, false, false, false, null);
	        
	        //exchangeur avec filtre pour permettre d'envoyer les données de la node attribué au client 
	        exchangeConnection = connection.createChannel();
			exchangeConnection.exchangeDeclare(CONNECTION_SENDER_NAME,"direct");
 
	        //class manipulant les nodes, leurs voisinages et leurs disponibilités
	        NodeManager n = new NodeManager(connection);
	        
	        //Gestion des demande de connexion
	        DeliverCallback deliverCallbackMessage = (consumerTag, delivery) -> {
	            String packet = new String(delivery.getBody(),"UTF-8");	//format du paquet : uniquement le filtre
	            String playerID = getUniqueID();
	            PlayerFilter.put(playerID, packet);
	            n.giveANodeToPlayer(playerID);
	        };
	        //exchangeConnection.basicPublish(CONNECTION_SENDER_NAME, packet, null, SerializationUtils.serialize(new PacketServer(node.getInChannelName(),node.getExchangeChannelName(),playerID)));//on envoie au client les liens vers les deux channel de la node
	        connectionRequest.basicConsume(CONNECTION_NAME, true, deliverCallbackMessage, consumerTag -> { });
	        
	        
	 }

	/**
	 * Fonction d'attribution d'ID uniques
	 *
	 * @return String
	 */
	 public static String getUniqueID() {
		 //TODO : rendre les ID vraiment uniques
		 return Double.toHexString(random.nextDouble());
	 }

	/**
	 * Fonction permettant d'envoyer un message à un joueur donné
	 *
	 * @throws IOException
	 */
	 public static void sendMessageToPlayer(String playerID, String message) throws IOException {
		 exchangeConnection.basicPublish(CONNECTION_SENDER_NAME, PlayerFilter.get(playerID), null, SerializationUtils.serialize(new PacketServer(message)));
	 }

	/**
	 * Fonction permettant de rediriger un joueur vers une Node
	 *
	 * @throws IOException
	 */
	 public static void sendPlayerToAnotherNode(String playerID, String inChannelName, String exchangeChannelName) throws IOException {
		 exchangeConnection.basicPublish(CONNECTION_SENDER_NAME, PlayerFilter.get(playerID), null, SerializationUtils.serialize(new PacketServer(inChannelName,exchangeChannelName,playerID)));//on envoie au client les liens vers les deux channel de la node
	 }
	 
}
