package Node;

import Launcher.Launcher;
import Packet.PacketNodeToManager;
import Packet.PacketNodeToManagerType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

public class NodeCreator {

	public static void main(String[] argv) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException {
		Channel inChannel, channelInit;

		String uri;
		if(argv.length > 0 && argv[0].equals("cloud")){
			uri = "amqp://sdwugoov:XPyB55TBXi7XU8yOYE2U9G8GRV4IXy50@chinook.rmq.cloudamqp.com/sdwugoov";
			System.out.println("Tentative de connection à un serveur RabbitMQ distant");
		} else {
			uri = "amqp://guest:guest@localhost";
			System.out.println("Tentative de connection à un serveur RabbitMQ local");
		}

		ConnectionFactory factory = new ConnectionFactory();
		factory.setUri(uri);

		Connection connection;
		try{
			connection = factory.newConnection();
		} catch (Exception e){
			if(argv.length > 0 && argv[0].equals("cloud")){
				System.out.println("Connection au serveur distant impossible, êtes vous connectés a internet ? le serveur est peut-être indisponible");
			} else {
				System.out.println("Connection au serveur local impossible, RabbitMQ ne tourne pas sur cette machine");
			}
			return;
		}
		System.out.println("Connecté !");


		inChannel = connection.createChannel();
		inChannel.queueDeclare(NodeManager.nameListener, false, false, false, null);
		inChannel.basicPublish("",NodeManager.nameListener,null, SerializationUtils.serialize(new PacketNodeToManager()));


		//enregistrement
		channelInit = connection.createChannel();
		channelInit.queueDeclare(NodeManager.nameInit, false, false, false, null);

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			channelInit.basicCancel(consumerTag);
			String packet =  new String(delivery.getBody(),"UTF-8");
			new Node(packet,connection);
			inChannel.basicPublish("",NodeManager.nameListener,null, SerializationUtils.serialize(new PacketNodeToManager(packet)));
		};
		channelInit.basicConsume(NodeManager.nameInit, true, deliverCallback, consumerTag -> { });

	}
}
