package Map;

import java.io.Serializable;

public class Map implements Serializable{
	private static final long serialVersionUID = 8930348100151518792L;
	private int width, heigth;
	private MapCase[][] tab;
	private boolean haveUP, haveDOWN, haveLEFT, haveRIGHT;
	
	public Map(int width, int heigth, boolean up, boolean down, boolean left, boolean right) {
		this.width = width;
		this.heigth = heigth;
		haveUP = up;
		haveDOWN = down;
		haveLEFT = left;
		haveRIGHT = right;
		tab = new MapCase[width][heigth];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < heigth; j++) {
				tab[i][j] = new MapCase(MapElement.EMPTY);
			}
		}
	}

	public void setDirections(boolean up, boolean down, boolean left, boolean right){
		haveUP = up;
		haveDOWN = down;
		haveLEFT = left;
		haveRIGHT = right;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeigth() {
		return heigth;
	}
	
	public MapCase getElementAtCoords(int x, int y) {
		return tab[x][y];
	}
	
	public void setElementsAtCoords(int x, int y, MapCase element) {
		tab[x][y] = element;
	}	

	public boolean getUP() {return haveUP;}

	public boolean getDOWN() {return haveDOWN;}

	public boolean getLEFT() {return haveLEFT;}

	public boolean getRIGHT() {return haveRIGHT;}

}
