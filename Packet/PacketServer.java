package Packet;

import java.io.Serializable;

public class PacketServer implements Serializable{
	private static final long serialVersionUID = 3503634320415369004L;
	PacketServerType type;
	String content;
	
	String inChannelName;
	String exchangeChannelName;
	String playerID;
	
	public PacketServer(String content) {
		this.type = PacketServerType.MESSAGE;
		this.content = content;
	}
	
	public PacketServer(String in, String out, String ID) {
		this.type = PacketServerType.NODE_CONNECTION;
		inChannelName = in;
		exchangeChannelName = out;
		playerID = ID;
	}
	
	public PacketServerType getType() {
		return type;
	}
	
	public String getContent() {
		return content;
	}
	
	public String getInChannelName() {
		return inChannelName;
	}
	
	public String getExchangeChannelName() {
		return exchangeChannelName;
	}
	
	public String getPlayerID() {
		return playerID;
	}
}
