package Packet;

import Map.Direction;

import java.io.Serializable;

public class Packet implements Serializable{
	private static final long serialVersionUID = 9108779363102530646L;
	PacketType type;
	Direction direction;
	String ID;
	String name;
	public Packet(PacketType type, String ID, String name) {
		this.type = type;
		this.ID = ID;
		this.name = name;
	}
	
	public void setDirection(Direction direction) {
		if(type != PacketType.DIRECTION) {
			System.err.println("ERROR : PACKET SHOULD BE 'DIRECTION' TYPE");
		} else {
			this.direction = direction;
		}
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	public PacketType getType() {
		return type;
	}
	
	public String getID() {
		return ID;
	}
	
	public String getName() {
		return name;
	}
}
