package Packet;

import Map.Direction;

import java.io.Serializable;

public class PacketNodeToManager implements Serializable {
	private static final long serialVersionUID = 3503634320415369004L;

	PacketNodeToManagerType type;
	String name;
	Direction direction;
	String ID,message,nodeName, inChannelName, outChannelName;
	int x,y;
	float r,g,b;
	boolean transfertOK;

	public PacketNodeToManager(String name, Direction direction, String ID, int x, int y, float r, float g, float b){
		this.type = PacketNodeToManagerType.TRANSFERT;
		this.name = name;
		this.direction = direction;
		this.ID = ID;
		this.x = x;
		this.y = y;
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public PacketNodeToManager(String ID, String name, boolean transfertOK, String inChannelName, String outChannelName){
		this.type = PacketNodeToManagerType.VALIDATION;
		this.ID = ID;
		this.name = name;
		this.transfertOK = transfertOK;
		this.inChannelName = inChannelName;
		this.outChannelName = outChannelName;
	}

	public PacketNodeToManager(String ID, String message){
		this.type = PacketNodeToManagerType.MESSAGE;
		this.ID = ID;
		this.message = message;
	}

	public PacketNodeToManager(boolean ok, String ID, String inChannelName, String outChannelName){
		this.transfertOK = ok;
		this.type = PacketNodeToManagerType.CONNEXION;
		this.ID = ID;
		this.inChannelName = inChannelName;
		this.outChannelName = outChannelName;
	}

	public PacketNodeToManager(){
		this.type = PacketNodeToManagerType.INIT;
	}

	public PacketNodeToManager(String name) {
		this.type = PacketNodeToManagerType.UPDATE;
		this.name = name;
	}


	public PacketNodeToManagerType getType(){
		return type;
	}

	public String getName(){
		return name;
	}

	public Direction getDirection(){
		return direction;
	}

	public String getID(){
		return ID;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	public float getR(){
		return r;
	}

	public float getG(){
		return g;
	}

	public float getB(){
		return b;
	}

	public boolean getTransfertOK(){
		return transfertOK;
	}

	public String getMessage(){
		return message;
	}

	public String getNodeName() { return nodeName; }

	public String getInChannelName() { return inChannelName;};

	public String getOutChannelName() { return outChannelName;};

}
