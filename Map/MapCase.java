package Map;

import java.io.Serializable;
import java.util.Random;


public class MapCase implements Serializable{

	private static final long serialVersionUID = -8750147280326001936L;
	MapElement e;
	String IDPlayer;
	String NamePlayer;
	float r,g,b;
	public MapCase(MapElement element) {
		e = element;
	}
	
	public MapCase(String ID, String name) {
		e = MapElement.PLAYER;
		IDPlayer = ID;
		NamePlayer = name;
		
		Random rand = new Random();
		r = rand.nextFloat();
		g = rand.nextFloat();
		b = rand.nextFloat();
		while( r < 0.5 && g < 0.5 && b < 0.5){
			r = rand.nextFloat();
			g = rand.nextFloat();
			b = rand.nextFloat();
		}
	}
	
	public MapElement getType() {
		return e;
	}
	
	public String getIDPlayer() {
		return IDPlayer;
	}
	
	public String getName() {
		return NamePlayer;
	}
	
	public float getR() {
		return r;
	}
	
	public float getG() {
		return g;
	}
	
	public float getB() {
		return b;
	}

	public void setRGB(float r, float g, float b){
		this.r = r;
		this.g = g;
		this.b = b;
	}

}
