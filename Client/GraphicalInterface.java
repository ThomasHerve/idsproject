package Client;

import java.io.IOException;

import Map.Direction;
import Map.Map;
import Map.MapElement;
import Map.MapCase;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class GraphicalInterface extends Application {
	private static GraphicalInterface c;
	static final double initialSize = 600;

	private static Client client;//reference vers le client, necessaire pour permettre d'envoyer des demandes au serveur via des inputs
	static GraphicsContext gc;

	private Scene scene;
	private Canvas canvas;
	private Label msg;
	private double lineWidth = 2;
	private double playerReduction = 2;
	private double percentageCanvas = 0.90f;
	private double heightOffset = 1.1d;

	//Text
	private static long msgLifeTime = 2000;//in ms
	private Thread thread;

	public static void startApp(Client clientMaster) {
		Thread thread = new Thread() {
			@Override
			public void run() {
				GraphicalInterface c = new GraphicalInterface();
				client = clientMaster;
				c.launchApp();
			}
		};
		thread.start();
	}


	public GraphicalInterface() {
		
	}
	
	public void launchApp() {
		Application.launch();
	}
	
	
	
	@Override
	public void start(Stage primaryStage) {
		// Créé la fenetre
		primaryStage.setTitle("IDSProject HERVE Thomas and SONZOGNI Benoît");
		Group root = new Group();
		primaryStage.setHeight(initialSize * heightOffset);
		primaryStage.setWidth(initialSize);
		canvas = new Canvas(initialSize, initialSize);
        gc = canvas.getGraphicsContext2D();
        scene = new Scene(root);
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.UP) {
            	try {
					client.sendDirectionToNode(Direction.UP);
				} catch (IOException e1) {
					handleException(e1);
				}
            }
            else if (e.getCode() == KeyCode.DOWN) {
            	try {
					client.sendDirectionToNode(Direction.DOWN);
				} catch (IOException e1) {
					handleException(e1);
				}
            }
            else if(e.getCode() == KeyCode.LEFT) {
            	try {
					client.sendDirectionToNode(Direction.LEFT);
				} catch (IOException e1) {
					handleException(e1);
				}
            } 
            else if(e.getCode() == KeyCode.RIGHT) {
            	try {
					client.sendDirectionToNode(Direction.RIGHT);
				} catch (IOException e1) {
					handleException(e1);
				}
            }
        });

        //message management, permet de recevoir les messages du serveur, même ceux reçu alors que l'interface n'était pas encore instancié
		MessageManager.registerInterface(this);
        client.setGI(this);

		primaryStage.widthProperty().addListener((obs,oldval,newval)->{
			canvas.setWidth((newval.doubleValue()-16));
		});

		primaryStage.heightProperty().addListener((obs,oldval,newval)->{
			canvas.setHeight((newval.doubleValue()-36) / heightOffset);
		});
		canvas.setHeight(canvas.getHeight() / heightOffset);

		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		msg = new Label();
		box.getChildren().addAll(canvas,msg);
		root.getChildren().add(box);

		//Name selection scene
		VBox nameBoxe = new VBox();
		Scene sceneName = new Scene(nameBoxe);
		Label labelName = new Label("Entrez un nom");
		TextField fieldName = new TextField();
		Button validateName = new Button("OK");
		nameBoxe.getChildren().addAll(labelName,fieldName,validateName);

		validateName.setOnAction((e)->{
			if(fieldName.getText().length() > 0){
				client.setName(fieldName.getText());
				primaryStage.setScene(scene);
				canvas.setWidth((primaryStage.getWidth()-16));
				canvas.setHeight((primaryStage.getHeight()-36) / heightOffset);
				try {
					client.initClient();
				} catch (Exception ex) {
					handleException(ex);
				}
			}
		});

        primaryStage.setScene(sceneName);
        primaryStage.show();
	}
	
	public void stop() {
		System.exit(0);
	}

	private double getCaseWidth(double mapWidth){
		return canvas.getWidth()/mapWidth;
	}

	private double getCaseHeight(double mapHeight){
		return canvas.getHeight()/mapHeight;
	}

	public void setMap(Map map) {
		//Mettre à jour la map
		if(gc != null) {
			Platform.runLater(()->{
				gc.setFill(Color.WHITE);
				gc.fillRect( 0,0, canvas.getWidth(), canvas.getHeight());


				double decalageWidth = canvas.getWidth() * ((1 - percentageCanvas)/2);
				double decalageHeight = canvas.getHeight() * ((1 - percentageCanvas)/2);
				double caseWidth = getCaseWidth(map.getWidth()) - (decalageWidth*2)/map.getWidth();
				double caseHeight = getCaseHeight(map.getHeigth()) - (decalageHeight*2)/map.getHeigth();

				for(int i = 0; i < map.getWidth(); i++) {
					for(int j = 0; j < map.getHeigth(); j++) {
						gc.setFill(Color.BLACK);
						gc.fillRect( caseWidth*i + decalageWidth, caseHeight*j + decalageHeight, caseWidth, caseHeight);
						gc.setFill(Color.WHITE);
						gc.fillRect( caseWidth*i + decalageWidth + lineWidth, caseHeight*j + decalageHeight + lineWidth, caseWidth - lineWidth, caseHeight - lineWidth);
						if(map.getElementAtCoords(i, j).getType() == MapElement.PLAYER) {
							gc.setFill(Color.BLACK);
							gc.fillOval(caseWidth*i + decalageWidth + playerReduction, caseHeight*j + decalageHeight + playerReduction, caseWidth - playerReduction, caseHeight - playerReduction);
							gc.setFill(new Color(map.getElementAtCoords(i, j).getR(),map.getElementAtCoords(i, j).getG(),map.getElementAtCoords(i, j).getB(),1));
							gc.fillOval(caseWidth*i + decalageWidth + playerReduction + lineWidth/2, caseHeight*j + decalageHeight + playerReduction + lineWidth/2, caseWidth - playerReduction - lineWidth, caseHeight - playerReduction - lineWidth);
							gc.setFill(Color.BLACK);
							String name = map.getElementAtCoords(i, j).getName();

							for(int k = name.length(); k < (name.length()%2== 0 ? 10:11);k += 2) {
								name = " " + name ;
							}
							gc.setFont(new Font("Verdana", getCaseWidth(map.getWidth())*map.getWidth()/60));
							gc.fillText(name,caseWidth*i + decalageWidth + playerReduction + lineWidth/2, caseHeight*(j+0.5) + decalageHeight + playerReduction + lineWidth/2);
						} else if(map.getElementAtCoords(i, j).getType() == MapElement.TREE){
							//TODO DESSINER UN VRAI ARBRE
							gc.setFill(Color.BLACK);
							gc.fillRect( caseWidth*i + decalageWidth, caseHeight*j + decalageHeight, caseWidth, caseHeight);
						}
					}
				}
				//bornes
				gc.setFill(Color.GREEN);
				double triangleSizeWidth = canvas.getWidth() * ((1 - percentageCanvas)/3);
				double triangleSizeHeight = canvas.getHeight() * ((1 - percentageCanvas)/3);
				if(map.getUP()) {
					gc.fillPolygon(new double[]{canvas.getWidth()/2-triangleSizeWidth,canvas.getWidth()/2,canvas.getWidth()/2+triangleSizeWidth},
							new double[]{triangleSizeHeight,0,triangleSizeHeight},3);
				}
				if(map.getDOWN()) {
					gc.fillPolygon(new double[]{canvas.getWidth()/2-triangleSizeWidth,canvas.getWidth()/2,canvas.getWidth()/2+triangleSizeWidth},
							new double[]{canvas.getHeight()-triangleSizeHeight,canvas.getHeight(),canvas.getHeight()-triangleSizeHeight},3);
				}
				if(map.getLEFT()) {
					gc.fillPolygon(new double[]{0,triangleSizeWidth,triangleSizeWidth},
							new double[]{canvas.getHeight()/2,canvas.getHeight()/2-triangleSizeHeight,canvas.getHeight()/2+triangleSizeHeight},3);
				}
				if(map.getRIGHT()) {
					gc.fillPolygon(new double[]{canvas.getWidth(),canvas.getWidth()-triangleSizeWidth,canvas.getWidth()-triangleSizeWidth},
							new double[]{canvas.getHeight()/2,canvas.getHeight()/2-triangleSizeHeight,canvas.getHeight()/2+triangleSizeHeight},3);
				}
			});
		}
	}
	
	public void serverMessage(String message) {
		//Msg handler
		if(thread != null){
			thread.stop();
		}
		thread = new Thread() {
			long beginTime = 0;
			long nextTime = 0;

			@Override
			public void run() {
				if (msg != null) {
						Platform.runLater(() -> {
							msg.setText(message);
						});
						beginTime = System.currentTimeMillis();
						nextTime = beginTime + msgLifeTime;
					 while (beginTime < nextTime) {
						 beginTime = System.currentTimeMillis();
					}
					Platform.runLater(() -> {
						msg.setText("");
					});
				}
			}
		};
		thread.setDaemon(true);
		thread.start();

	}
	
	private void handleException(Exception e) {
		e.printStackTrace();
	}

}
