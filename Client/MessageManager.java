package Client;

import java.util.LinkedList;

public class MessageManager {
	private static LinkedList<String> messages = new LinkedList<>();
	private static GraphicalInterface gi;
	public static void addMessage(String msg){
		if(gi == null)
		messages.add(msg);
		else gi.serverMessage(msg);
	}

	public static boolean haveMessage(){
		return messages.size() > 0;
	}

	public static String getMessage() {
		String m = messages.get(0);
		messages.remove(0);
		return m;
	}

	public static void registerInterface(GraphicalInterface gi){
		while(haveMessage()){
			gi.serverMessage(getMessage());
		}
		MessageManager.gi = gi;
	}
}
