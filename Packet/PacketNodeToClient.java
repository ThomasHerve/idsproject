package Packet;

import Map.Map;

import java.io.Serializable;

public class PacketNodeToClient implements Serializable {
	private static final long serialVersionUID = 8930348100136718792L;

	PacketNodeToClientType type;
	Map currentMap;

	public PacketNodeToClient(){
		type = PacketNodeToClientType.VERIFICATION;
	}

	public PacketNodeToClient(Map map){
		currentMap = map;
		type = PacketNodeToClientType.MAP;
	}

	public PacketNodeToClientType getType(){
		return type;
	}

	public Map getMap(){
		return currentMap;
	}


}
