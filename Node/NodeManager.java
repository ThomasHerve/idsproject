package Node;

import Client.GraphicalInterface;
import Launcher.Launcher;
import Map.Direction;
import Packet.Packet;
import Packet.PacketManagerToNode;
import Packet.PacketNodeToManager;
import Packet.PacketNodeToManagerType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.sun.xml.internal.bind.v2.model.core.ID;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

//classe dont l'objectif est de gérer le voisinage entre les nodes
public class NodeManager {
	public static String nameListener = "NodeManagerListener";
	public static String nameExchanger = "NodeManagerExchanger";
	public static String nameInit = "NodeManagerExchangerInit";

	ArrayList<String> nodeNameList = new ArrayList<String>();
	HashMap<String,Integer> playerWaitingForNode = new HashMap<>();
	String[][] mapNode;
	boolean[][] pattern;
	Connection connection;
	Channel inChannel, exchangeChannel, channelInit;
	int count = 0;

	/**
	 *
	 * Notre manager de nodes, ce dernier va les créer dans des threads et ouvrir des channels pour pouvoir communiquer avec
	 *
	 * @param connection
	 * 			La classe Connection sur laquelle on va créer les channels RabbitMQ
	 *
	 * @throws IOException
	 */
	public NodeManager(Connection connection) throws IOException {
		this.connection = connection;
		generateMapPattern();

		initListener();
		initExchanger();

	}


	//permet d'envoyer une demande de connection à la node courante selon le joueur (on les essaie toute jusqu'a en trouver une libre)
	public void giveANodeToPlayer(String ID) throws IOException {
		if(playerWaitingForNode.get(ID) == null) {
			playerWaitingForNode.put(ID,0);
		}  if(playerWaitingForNode.get(ID) >= nodeNameList.size()) {
			Launcher.sendMessageToPlayer(ID,"Connection impossible, le serveur est plein");
			playerWaitingForNode.put(ID,0);
			return;
		}

		if(nodeNameList.size() > playerWaitingForNode.get(ID)){
			exchangeChannel.basicPublish(nameExchanger, nodeNameList.get(playerWaitingForNode.get(ID)), null, SerializationUtils.serialize(new PacketManagerToNode(ID)));
		}
	}

	//pour l'instant la carte est hard codé, peut etre que l'on ajoutera la possibilité de lire un fichier à la place
	private void generateMapPattern(){
		pattern = new boolean[][]{
				{false,true,false},
				{true,true,true},
				{false,true,false}
		};
		mapNode = new String[][]{
				{"","",""},
				{"","",""},
				{"","",""}
		};
	}


	/**
	 *
	 * Permet d'écouter les demandes des nodes, tel que des transferts ou envoyer un message vers un joueur precis
	 *
	 * @throws IOException
	 */
	private void initListener() throws IOException {
		inChannel = connection.createChannel();
		inChannel.queueDeclare(nameListener, false, false, false, null);

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			PacketNodeToManager packet =  (PacketNodeToManager) SerializationUtils.deserialize(delivery.getBody());
			if(packet.getType() == PacketNodeToManagerType.TRANSFERT){
				transfertPlayer(packet.getName(),packet.getDirection(),packet.getID(),packet.getX(),packet.getY(),packet.getR(),packet.getG(),packet.getB());
			} else if(packet.getType() == PacketNodeToManagerType.VALIDATION){
				if(packet.getTransfertOK()){
					//la node a confirmé que la place est libre, on peu renvoyer au client sa nouvelle node
					Launcher.sendPlayerToAnotherNode(packet.getID(),packet.getInChannelName(),packet.getOutChannelName());
				} else {
					Launcher.sendMessageToPlayer(packet.getID(),"Deplacement impossible, la case est occupé par un obstacle ou par un autre joueur");
				}
			} else if(packet.getType() == PacketNodeToManagerType.MESSAGE){
				Launcher.sendMessageToPlayer(packet.getID(),packet.getMessage());
			} else if(packet.getType() == PacketNodeToManagerType.INIT) {
				//enregitrer la node
				for(int i = 0; i < pattern.length; i++) {
					for(int j = 0; j < pattern[0].length; j++){
						if(pattern[i][j] && mapNode[i][j] == ""){
							String name = new Integer(nodeNameList.size()).toString();
							channelInit.basicPublish("",nameInit,null,name.getBytes());
							mapNode[i][j] = name;
							nodeNameList.add(name);
							return;
						}
					}
				}
			} else if(packet.getType() == PacketNodeToManagerType.CONNEXION) {
				if(packet.getTransfertOK()) {
					//On peut rediriger le joueur vers la node
					Launcher.sendPlayerToAnotherNode(packet.getID(),packet.getInChannelName(),packet.getOutChannelName());
				} else {
					//On ne peut pas
					playerWaitingForNode.put(packet.getID(),playerWaitingForNode.get(packet.getID())+1);
					giveANodeToPlayer(packet.getID());
				}
			} else if(packet.getType() == PacketNodeToManagerType.UPDATE) {
				//demande de mise à jour des voisins
				for(int a = 0; a < pattern.length; a++) {
					for(int b = 0; b < pattern[0].length; b++) {
						if(mapNode[a][b] != "")
							exchangeChannel.basicPublish(nameExchanger, mapNode[a][b], null, SerializationUtils.serialize(new PacketManagerToNode(a > 0 && mapNode[a-1][b] != "",a < pattern.length-1 && mapNode[a+1][b] != "",b > 0 && mapNode[a][b-1] != "",b < pattern[0].length-1 && mapNode[a][b+1] != "")));
					}
				}
			}
		};
		inChannel.basicConsume(nameListener, true, deliverCallback, consumerTag -> { });
	}

	private void initExchanger() throws IOException{
		exchangeChannel = connection.createChannel();
		exchangeChannel.exchangeDeclare(nameExchanger, "direct");
		channelInit = connection.createChannel();
		channelInit.queueDeclare(nameInit, false, false, false, null);
	}

	//permet de transmettre à une node le fait qu'un joueur souhaite changer de carte, permet de savoir si la case de deplacement est accessible
	private void transfertPlayer(String name, Direction dir, String ID, int x, int y, float r, float g, float b) throws IOException {
		String newNodeName = getNodeNeighbourg(name,dir);
		switch(dir){
			case UP:
				y = Node.SIZE-1;
				break;
			case DOWN:
				y = 0;
				break;
			case LEFT:
				x = Node.SIZE-1;
				break;
			default:
				x = 0;
				break;
		}

		//envoie de la demande de transfert à la node
		exchangeChannel.basicPublish(nameExchanger, newNodeName, null, SerializationUtils.serialize(new PacketManagerToNode(ID,x,y,r,g,b)));
	}

	/**
	 *
	 * Permet d'obtenir la node voisine d'une autre node selon une direction
	 *
	 * @param name
	 * 			nom de la node
	 * @param dir
	 * 			direction où trouver la node voisine
	 *
	 * @return Node
	 */
	private String getNodeNeighbourg(String name,Direction dir){

		int x = 0,y = 0;
		for(int i = 0; i < pattern.length; i++){
			for(int j = 0; j < pattern[0].length; j++){
				if(name.equals(mapNode[i][j])){
					x = i;
					y = j;
					break;
				}
			}
		}
		switch(dir){
			case UP:
				return mapNode[x-1][y];
			case DOWN:
				return mapNode[x+1][y];
			case LEFT:
				return mapNode[x][y-1];
			default:
				return mapNode[x][y+1];
		}
	}


}
