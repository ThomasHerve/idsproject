package Packet;

import java.io.Serializable;

public class PacketManagerToNode implements Serializable {
	private static final long serialVersionUID = 3503634320415369004L;

	PacketManagerToNodeType type;
	String ID;
	int x,y;
	float r,g,b;
	boolean up,down,left,right;

	public PacketManagerToNode(String ID, int x, int y, float r, float g, float b){
		type = PacketManagerToNodeType.TRANSFERT;
		this.ID = ID;
		this.x = x;
		this.y = y;
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public PacketManagerToNode(String ID) {
		type = PacketManagerToNodeType.CONNECTION;
		this.ID = ID;
	}

	public PacketManagerToNode(boolean up,boolean down,boolean left,boolean right){
		type = PacketManagerToNodeType.UPDATE;
		this.up = up;
		this.down = down;
		this.left = left;
		this.right = right;
	}

	public PacketManagerToNodeType getType() { return type; }

	public String getID(){
		return ID;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return y;
	}

	public float getR(){
		return r;
	}

	public float getG(){
		return g;
	}

	public float getB(){
		return b;
	}

	public boolean isUp() {
		return up;
	}

	public boolean isDown() {
		return down;
	}

	public boolean isLeft() {
		return left;
	}

	public boolean isRight() {
		return right;
	}
}

