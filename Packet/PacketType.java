package Packet;

public enum PacketType {
	DIRECTION, ALIVE_VERIFICATION, CONNECTION
}
