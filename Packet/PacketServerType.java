package Packet;

public enum PacketServerType {
	NODE_CONNECTION, MESSAGE, ERROR
}
