package Client;
import Launcher.Launcher;
import Map.Map;
import Map.Direction;
import Packet.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;
import java.util.concurrent.TimeoutException;

import Map.Map;
import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;


public class Client {
	 String argv[];
	 GraphicalInterface gi;

	/**
	 * Fonction d'entrée du client, argument : aucun pour se connecter à un serveur RabbitMQ local, "cloud" pour se connecter a un serveur distant
	 *
	 */
	 public static void main(String[] argv) throws Exception {
		new Client(argv);
	 }
	 
	 Connection connection;
	 Channel sendChannel, getChannel;
	 String sendChannelName;
	 String ID;//identifiant necessaire pour que la node sache quelle client nous sommes
	 String name = "";
	 Map currentMap;

	/**
	 * Constructeur du client, permet d'instancier l'interface graphique
	 *
	 */
	 public Client(String[] argv) throws IOException, TimeoutException {
		 //interface graphique
		 GraphicalInterface.startApp(this);
		this.argv = argv;
	 }

	/**
	 * initialisation du client, appelé par l'interface graphique lorsque cette derniere a fini de s'initialiser
	 * C'est dans cette fonction que le client se connecte au serveur RabbitMQ
	 */
	 public void initClient() throws IOException, TimeoutException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException {
		//connection
		 String uri;
		 if(argv.length > 0 && argv[0].equals("cloud")){
			 uri = "amqp://sdwugoov:XPyB55TBXi7XU8yOYE2U9G8GRV4IXy50@chinook.rmq.cloudamqp.com/sdwugoov";
			 System.out.println("Tentative de connection à un serveur RabbitMQ distant");
		 } else {
			 uri = "amqp://guest:guest@localhost";
			 System.out.println("Tentative de connection à un serveur RabbitMQ local");
		 }
		 ConnectionFactory factory = new ConnectionFactory();
		 factory.setUri(uri);
		 try{
			 connection = factory.newConnection();
		 } catch (Exception e){
			 if(argv.length > 0 && argv[0].equals("cloud")){
			 	String msg = "Connection au serveur distant impossible, êtes vous connectés a internet ? le serveur est peut-être indisponible";
				 System.out.println(msg);
				 gi.serverMessage(msg);
			 } else {
			 	String msg = "Connection au serveur local impossible, RabbitMQ ne tourne pas sur cette machine";
				 System.out.println(msg);
				 gi.serverMessage(msg);
			 }
			 return;
		 }
		 System.out.println("Connecté !");

		 //channel de reception de connection, permet de recevoir les coordonnées de la node
		 Channel channelConnection = connection.createChannel();
		 String channelFilter = Double.toHexString(new Random().nextDouble());//temporaire, il faut garantir l'unicité
		 channelConnection.exchangeDeclare(Launcher.CONNECTION_SENDER_NAME, "direct");
		 String queueNameConnection = channelConnection.queueDeclare().getQueue();
		 channelConnection.queueBind(queueNameConnection, Launcher.CONNECTION_SENDER_NAME, channelFilter);

		 DeliverCallback deliverCallbackConnection = (consumerTag, delivery) -> {

			 PacketServer packet = (PacketServer)SerializationUtils.deserialize(delivery.getBody());
			 switch(packet.getType()) {
				 case NODE_CONNECTION:
					 ID = packet.getPlayerID();
					 try {
						 connectedToNode(packet.getInChannelName(),packet.getExchangeChannelName());
					 } catch (TimeoutException e) {
						 e.printStackTrace();
					 }
					 break;
				 case MESSAGE:
					 MessageManager.addMessage(packet.getContent());
					 break;
				 default:
					 break;
			 }
		 };
		 channelConnection.basicConsume(queueNameConnection, true, deliverCallbackConnection, consumerTag -> { });

		 //Channel pour emettre la premiere demande de connexion
		 Channel connectionRequest = connection.createChannel();
		 connectionRequest.queueDeclare(Launcher.CONNECTION_NAME, false, false, false, null);
		 connectionRequest.basicPublish("", Launcher.CONNECTION_NAME, null, channelFilter.getBytes());
	 }

	 //c'est l'interface graphique qui se donne au moteur, cela est lié aux fait que les toolkits sont sur d'autres threads et mettent du temps à s'instancier
	 public void setGI(GraphicalInterface gi){
	 	this.gi = gi;
	 }
	 
	 /**
		 * La fonction appellée pour se connecter à une node
		 * @param sendChannelName
		 * 			le nom du channel distant dans lequel envoyer des informations
		 * @param getChannelName
		 * 			le nom du channel distant dans lequel obtenir des informations
		 * 
		 */
	 public void connectedToNode(String sendChannelName, String getChannelName) throws IOException, TimeoutException {
		 //nous avons ici les informations sur la node, cette fonction peut etre appeller plusieur fois au cours de la
		 //vie du client, il faut donc faire en sorte que les channels soit global pour permettre un changement de node

		 if(sendChannel != null) {
			 sendChannel.close();
			 getChannel.close();
		 }

		 //on declare la queue d'envoie
		 sendChannel = connection.createChannel();
		 sendChannel.queueDeclare(sendChannelName, false, false, false, null);
		 this.sendChannelName = sendChannelName;


		 //on declare la queue de reception
		 getChannel = connection.createChannel();
		 getChannel.exchangeDeclare(getChannelName, "fanout");
		 String queueName = getChannel.queueDeclare().getQueue();
		 getChannel.queueBind(queueName, getChannelName, "");
		 
		 DeliverCallback deliverCallback = (consumerTag, delivery) -> {
		 		PacketNodeToClient packet = (PacketNodeToClient)SerializationUtils.deserialize(delivery.getBody());
		 		if(packet.getType() == PacketNodeToClientType.MAP) {
					//mise à jour graphique
					if (gi != null) {
						gi.setMap(packet.getMap());
					}
				}

			 	//on confirme que l'on est encore connectés
			 	try {
					sendChannel.basicPublish("", sendChannelName, null, SerializationUtils.serialize(new Packet(PacketType.ALIVE_VERIFICATION, ID, name)));
				} catch (Exception e){}
		 };
		 getChannel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });

		 //On se connecte à la node
		 sendChannel.basicPublish("",sendChannelName, null, SerializationUtils.serialize(new Packet(PacketType.CONNECTION,ID,name)));

	 }

	/**
	 * La fonction appellée pour envoyer une direction à la node courante
	 * @param direction
	 * 			la direction envoyé à la node
	 */
	 public void sendDirectionToNode(Direction direction) throws IOException {
		 Packet p = new Packet(PacketType.DIRECTION,ID,name);
		 p.setDirection(direction);
		 try {
		 	sendChannel.basicPublish("",sendChannelName, null, SerializationUtils.serialize(p));
		 } catch (Exception e){}
	 }

	/**
	 * La fonction utilisée par l'interface graphique pour choisir un nom
	 * @param n
	 * 			le nom choisi
	 */
	 public void setName(String n){
	 	name = n;
	 }
}
