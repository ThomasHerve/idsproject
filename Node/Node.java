package Node;
import Map.Direction;
import Map.Coords;
import Map.MapCase;
import Map.Map;
import Map.MapElement;
import Packet.Packet;
import Packet.PacketNodeToManager;
import Packet.PacketManagerToNode;
import Packet.PacketManagerToNodeType;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

import Launcher.Launcher;
import Packet.PacketNodeToClient;
import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

public class Node {
	public static int SIZE = 10;
	private static String playerExchangerName = "playerExchanger";

	//gestion du jeu
	Map tab;
	LinkedList<String> players = new LinkedList<String>();
	LinkedList<Coords> playerToCome = new LinkedList<>();
	HashMap<String,Boolean> playerState = new HashMap<>();
	private boolean haveUP, haveDOWN, haveLEFT, haveRIGHT;//savoir si l'on a un voisin dans une direction donnée

	//gestion de la connexion
	Connection connection;
	String name;
	
	//gestion de la synchronisation
	ReentrantLock lock = new ReentrantLock();
	
	private String inChannelName;
	private Channel inChannel;//channel qui s'occupe de recuperer les demandes de déplacement, tout les clients publient dedans et la node considere si le deplacement est possible
	private String exchangeChannelName;
	private Channel exchangeChannel;//Exchangeur qui permet de publier le nouvel état de la carte à tout les clients
	private Channel managerPublisher;//Channel utilisé pour envoyer des paquets au NodeManager

	//utilitaire
	Random random = new Random();

	/**
	 * 
	 * Une node de notre systeme, contient une carte et des voisins, peut acceuillir des clients
	 * 
	 *
	 * @param name
	 * 			Le nom (unique) de la node
	 *
	 * @param connection
	 * 			La classe Connection sur laquelle on va créer les channels RabbitMQ
	 *
	 * @throws IOException 
	 */
	public Node(String name, Connection connection) throws IOException {
		this.connection = connection;
		this.name = name;

		//initialisation de la carte
		initTab();
		
		//initialisation des Channels
		initListener();
		initPublisher();
		initManagerPublisher();
		initManagerListener();

		//Creation d'un Timer qui envoie les informations de maniere reguliere
		initTimer();
	}
	
	
	
	//methodes publique
	/**
	 * Permet d'obtenir le nom du channel d'ecoute
	 *
	 * @return String
	 */
	 public String getInChannelName() {
		return inChannelName;
	}

	/**
	 * Permet d'obtenir le nom du channel d'envoi
	 *
	 * @return String
	 */
	public String getExchangeChannelName() {
		return exchangeChannelName;
	}
	
	
	//methodes privé
	
	//reseau
	/**
	 * Initialise la queue d'envoi
	 *
	 * @throws IOException
	 */
	private void initPublisher() throws IOException {
		exchangeChannelName = name + "Publisher";
		exchangeChannel = connection.createChannel();
		exchangeChannel.exchangeDeclare(exchangeChannelName, "fanout");


	}

	/**
	 * Initialise la queue d'ecoute
	 *
	 * @throws IOException
	 */
	private void initListener() throws IOException {
		inChannelName = name + "Listener";
		inChannel = connection.createChannel();
		inChannel.queueDeclare(inChannelName, false, false, false, null);

		DeliverCallback deliverCallback = (consumerTag, delivery) -> {
			//On recois un string contenant une direction 
			lock.lock();
			
			//TODO : obtenir packet
			Packet p = (Packet)SerializationUtils.deserialize(delivery.getBody());
			switch(p.getType()) {
				case CONNECTION:
					if(!players.contains(p.getID())) {
						players.add(p.getID());
						Coords c;
						if((c = playerToComeContainID(p.getID())) != null){
							setInitialCase(p.getID(),p.getName(),c.getX(),c.getY());
						} else {
							setInitialCase(p.getID(),p.getName());
						}
						sendPacketMap();
					}
					break;
				case DIRECTION:
					if(players.contains(p.getID())) {
						if(isMovingPossible(p.getID(),p.getDirection())) {
							Coords coords = getCoordOfPlayer(p.getID());
							//on commence par retirer le joueur de sa case actuelle
							MapCase mapCase = tab.getElementAtCoords(coords.getX(),coords.getY());
							tab.setElementsAtCoords(coords.getX(),coords.getY(),new MapCase(MapElement.EMPTY));
							//On sait que l'on peut se deplacer sur la case de la direction donnée
							switch(p.getDirection()) {
							case UP:
								tab.setElementsAtCoords(coords.getX(),coords.getY()-1,mapCase);
								sayHello(p.getID());
								break;
							case DOWN:
								tab.setElementsAtCoords(coords.getX(),coords.getY()+1,mapCase);
								sayHello(p.getID());
								break;
							case LEFT:
								tab.setElementsAtCoords(coords.getX()-1,coords.getY(),mapCase);
								sayHello(p.getID());
								break;
							case RIGHT:
								tab.setElementsAtCoords(coords.getX()+1,coords.getY(),mapCase);
								sayHello(p.getID());
								break;
							}
							sendPacketMap();
						}
					}
					break;
				case ALIVE_VERIFICATION:
					if(players.contains(p.getID())) {
						playerState.put(p.getID(),true);
					}
					break;
			}
			
			lock.unlock();
        };
        
        inChannel.basicConsume(inChannelName, true, deliverCallback, consumerTag -> { });
	}

	/**
	 * Initialise la queue d'envoi vers le manager
	 *
	 * @throws IOException
	 */
	private void initManagerPublisher() throws IOException {
		managerPublisher = connection.createChannel();
		managerPublisher.queueDeclare(NodeManager.nameListener, false, false, false, null);
	}

	/**
	 * Initialise la queue d'ecoute vers le manager
	 *
	 * @throws IOException
	 */
	private void initManagerListener() throws IOException {
		Channel managerListener = connection.createChannel();
		managerListener.exchangeDeclare(NodeManager.nameExchanger, "direct");
		String queueNameConnection = managerListener.queueDeclare().getQueue();
		managerListener.queueBind(queueNameConnection, NodeManager.nameExchanger, name);
		DeliverCallback deliverCallbackConnection = (consumerTag, delivery) -> {
			PacketManagerToNode packet = (PacketManagerToNode)SerializationUtils.deserialize(delivery.getBody());
			if(packet.getType() == PacketManagerToNodeType.CONNECTION){
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(havePlaceForANewPlayer(),packet.getID(),inChannelName,exchangeChannelName)));
			} else if(packet.getType() == PacketManagerToNodeType.TRANSFERT) {
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(packet.getID(),name,addPlayerToCome(packet.getID(), packet.getX(), packet.getY(), packet.getR(), packet.getG(), packet.getB()),inChannelName,exchangeChannelName)));
			} else if(packet.getType() == PacketManagerToNodeType.UPDATE){
				haveUP = packet.isUp();
				haveDOWN = packet.isDown();
				haveLEFT = packet.isLeft();
				haveRIGHT = packet.isRight();
				tab.setDirections(haveUP,haveDOWN,haveLEFT,haveRIGHT);
			}
		};
		managerListener.basicConsume(queueNameConnection, true, deliverCallbackConnection, consumerTag -> { });
	}


	/**
	 * permet de dire bonjour si on s'est deplacé sur une case à coté d'un autre joueur
	 * @param ID
	 * 			L'ID du joueur
	 * @throws IOException
	 */
	private void sayHello(String ID) throws IOException {
		Coords c = getCoordOfPlayer(ID);
		String name = tab.getElementAtCoords(c.getX(),c.getY()).getName();
		if(c.getY() > 0) {//UP
			if(tab.getElementAtCoords(c.getX(),c.getY()-1).getType() == MapElement.PLAYER){
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(tab.getElementAtCoords(c.getX(),c.getY()-1).getIDPlayer(),name +" vous dis bonjour!")));
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(ID,"Vous avez dis bonjour à " +tab.getElementAtCoords(c.getX(),c.getY()-1).getName()+" !")));

			}
		}
		if(c.getY() < tab.getHeigth()-1) {//DOWN
			if(tab.getElementAtCoords(c.getX(),c.getY()+1).getType() == MapElement.PLAYER){
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(tab.getElementAtCoords(c.getX(),c.getY()+1).getIDPlayer(),name +" vous dis bonjour!")));
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(ID,"Vous avez dis bonjour à " +tab.getElementAtCoords(c.getX(),c.getY()+1).getName()+" !")));

			}
		}
		if(c.getX() > 0) {//LEFT
			if(tab.getElementAtCoords(c.getX()-1,c.getY()).getType() == MapElement.PLAYER){
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(tab.getElementAtCoords(c.getX()-1,c.getY()).getIDPlayer(),name +" vous dis bonjour!")));
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(ID,"Vous avez dis bonjour à " +tab.getElementAtCoords(c.getX()-1,c.getY()).getName()+" !")));

			}
		}
		if(c.getX() < tab.getWidth()-1) {//RIGHT
			if(tab.getElementAtCoords(c.getX()+1,c.getY()).getType() == MapElement.PLAYER){
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(tab.getElementAtCoords(c.getX()+1,c.getY()).getIDPlayer(),name +" vous dis bonjour!")));
				managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(ID,"Vous avez dis bonjour à " +tab.getElementAtCoords(c.getX()+1,c.getY()).getName()+" !")));
			}
		}
	}

	//
	/**
	 * initialise la carte, on fait le choix de mettre quelques arbres aléatoirement pour differencier les cartes
	 */
	private void initTab() {
		tab = new Map(SIZE,SIZE,haveUP,haveDOWN,haveLEFT,haveRIGHT);
		int x =  random.nextInt(SIZE);
		int y =  random.nextInt(SIZE);
		for(int i = 0; i < random.nextInt(3)+1; i++){
			while(tab.getElementAtCoords(x,y).getType() != MapElement.EMPTY){
				x =  random.nextInt(SIZE);
				y =  random.nextInt(SIZE);
			}
			tab.setElementsAtCoords(x, y, new MapCase(MapElement.TREE));
		}
	}

	/**
	 * initialise le timer global qui sert à envoyer a chaque joueur un paquet de verification, pour être sûr qu'il est encore connecté
	 */
	private void initTimer() {
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
	        @Override
	        public void run() {
	        	try {
					sendPacket();
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }}, 100, 100);
	}

	/**
	 * fonction servant à verifier pour chaque joueur qu'il est bien encore connecté. Ces dernier doivent envoyer un paquet "ALIVE_VERIFICATION" regulierement, si il ne le font pas entre deux
	 * passes de verification alors ils seront retirés du jeu car considerés comme deconnectés
	 */
	private void initPersonnalTimer(String ID) {
		playerState.put(ID, true);
		Timer timer = new Timer();
		timer.schedule(new TimerTask(){
			@Override
			public void run() {
				if(playerState.get(ID)){
					playerState.put(ID,false);
				} else {
					removePlayer(ID);
					this.cancel();
				}
			}}, 1000, 500);
	}

	/**
	 * fonction servant à retirer un joueur de la node
	 */
	private void removePlayer(String ID) {
		playerState.remove(ID);
		players.remove(ID);
		Coords c = getCoordOfPlayer(ID);
		tab.setElementsAtCoords(c.getX(),c.getY(),new MapCase(MapElement.EMPTY));
	}

	/**
	 * permet d'envoyer un paquet de verification à tout les joueurs
	 *
	 * @throws IOException
	 */
	private void sendPacket() throws IOException {
		lock.lock();
		exchangeChannel.basicPublish(exchangeChannelName, "", null, SerializationUtils.serialize(new PacketNodeToClient()));
		lock.unlock();
	}

	/**
	 * permet d'envoyer un paquet de mise à jour de la carte à tout les joueurs
	 *
	 * @throws IOException
	 */
	private void sendPacketMap() throws IOException {
		lock.lock();
		exchangeChannel.basicPublish(exchangeChannelName, "", null, SerializationUtils.serialize(new PacketNodeToClient(tab)));
		lock.unlock();
	}
	
	//jeu

	/**
	 * renvoie true si on a la place d'acceuillir un joueur de plus
	 */
	public boolean havePlaceForANewPlayer(){
		int maxplayer = 2;//(new Double(Math.sqrt(tab.getWidth()*tab.getHeigth()))).intValue();
		return maxplayer > players.size();
	}

	/**
	 * permet de d'obtenir une case vide aléatoire
	 * @param ID
	 * 			L'ID du joueur
	 * @param name
	 * 			Le pseudo du joueur
	 * @throws IOException
	 */
	private boolean setInitialCase(String ID, String name) throws IOException {
		initPersonnalTimer(ID);
		//On sait que il y a forcemment une place libre grâce au launcher, on peut se permettre de chercher une case de maniere aléatoire
		int x = SIZE/2;
		int y = SIZE/2;
		while (tab.getElementAtCoords(x,y).getType() != MapElement.EMPTY){
			x =  random.nextInt(SIZE);
			y =  random.nextInt(SIZE);
		}
		tab.setElementsAtCoords(x, y, new MapCase(ID,name));
		sayHello(ID);
		return true;
	}

	/**
	 * permet de d'obtenir une case pour un joueur venant d'une autre node
	 * @param ID
	 * 			L'ID du joueur
	 * @param name
	 * 			Le pseudo du joueur
	 * @param x
	 * 			coordonnée x
	 * @param y
	 * 			coordonnée y
	 * @throws IOException
	 */
	private boolean setInitialCase(String ID, String name, int x, int y) throws IOException {
		initPersonnalTimer(ID);
		tab.setElementsAtCoords(x, y, new MapCase(ID,name));
		sayHello(ID);
		for(Coords c : playerToCome){
			if(c.getID().equals(ID)){
				tab.getElementAtCoords(x,y).setRGB(c.getR(),c.getG(),c.getB());
				playerToCome.remove(c);
			}
		}
		return true;
	}

	/**
	 * permet de d'obtenir les coordonnées d'un joueur en attente a partir de son ID
	 * @param ID
	 * 			L'ID du joueur
	 * @return Coords
	 */
	private Coords playerToComeContainID(String ID){
		for(Coords c : playerToCome){
			if(c.getID().equals(ID)){
				return c;
			}
		}
		return null;
	}

	/**
	 * permet d'ajouter un joueur venant d'une autre node
	 * @param ID
	 * 			L'ID du joueur
	 * @param x
	 * 			La coordonnée x d'arrivée
	 * @param y
	 * 			La coordonnée y d'arrivée
	 * @param r
	 * 			la composante r de la couleur du joueur
	 * @param g
	 * 	 		la composante g de la couleur du joueur
	 * @param b
	 * 	 		la composante b de la couleur du joueur
	 * @return boolean, true si la joueur a été ajouté, false sinon (case occupée)
	 */
	public boolean addPlayerToCome(String ID, int x, int y, float r, float g, float b){
		lock.lock();
		if(players.contains(ID)){
			lock.unlock();
			return false;
		}
		/*
		if(tab.getElementAtCoords(x,y).getType() == MapElement.PLAYER && tab.getElementAtCoords(x,y).getIDPlayer().equals(ID)){
			playerState.put(ID,true);
			lock.unlock();
			return true;
		}*/
		if(tab.getElementAtCoords(x,y).getType() != MapElement.EMPTY){
			lock.unlock();
			return false;
		}
		if(!players.contains(ID)){
			Coords c = new Coords(x,y);
			c.setID(ID);
			c.setRGB(r,g,b);
			tab.setElementsAtCoords(x, y, new MapCase(ID,name));
			playerToCome.add(c);
		}
		lock.unlock();
		return true;
	}

	/**
	 * permet d'obtenir les coordonnées d'un joueur à partir de son ID
	 * @param ID
	 * 			L'ID du joueur
	 *
	 * @return Coords
	 */
	private Coords getCoordOfPlayer(String ID) {
		//permet de connaitre la coordonnée d'un joueur dans la carte
		for(int i = 0; i < tab.getWidth(); i++) {
			for(int j = 0; j < tab.getHeigth(); j++) {
				if(tab.getElementAtCoords(i, j).getType() == MapElement.PLAYER && tab.getElementAtCoords(i, j).getIDPlayer().equals(ID)) {
					Coords c = new Coords(i,j);
					c.setRGB(tab.getElementAtCoords(i, j).getR(),tab.getElementAtCoords(i, j).getG(),tab.getElementAtCoords(i, j).getB());
					return c;
				}
			}
		}
		return null;
	}

	/**
	 * permet de savoir si un deplacement est réalisable, de plus si le joueur sort de la carte est peux aller vers une autre node, opere le transfert vers cette derniere
	 * @param ID
	 * 			L'ID du joueur
	 * @param direction
	 * 			La direction ou le joueur veux aller
	 *
	 * @return boolean
	 * @throws IOException
	 */
	private boolean isMovingPossible(String ID, Direction direction) throws IOException {
		Coords c = getCoordOfPlayer(ID);
		if(c == null)return false;
		switch (direction) {
			case UP:
				if(c.getY() == 0 && haveUP) {
					//nodeManager.transfertPlayer(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB())
					managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB())));
					return false;
				}
				return c.getY() > 0 && tab.getElementAtCoords(c.getX(), c.getY() - 1).getType() == MapElement.EMPTY;
			case DOWN:
				if(c.getY() ==  tab.getHeigth() - 1 && haveDOWN) {
					//nodeManager.transfertPlayer(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB());
					managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB())));
					return false;
				}
				return c.getY() < tab.getHeigth() - 1 && tab.getElementAtCoords(c.getX(), c.getY() + 1).getType() == MapElement.EMPTY;
			case LEFT:
				if(c.getX() == 0 && haveLEFT) {
					//nodeManager.transfertPlayer(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB());
					managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB())));
					return false;
				}
				return c.getX() > 0 && tab.getElementAtCoords(c.getX() - 1, c.getY()).getType() == MapElement.EMPTY;	
			default:
				if(c.getX() ==  tab.getWidth() - 1 && haveRIGHT) {
					//nodeManager.transfertPlayer(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB());
					managerPublisher.basicPublish("", NodeManager.nameListener, null, SerializationUtils.serialize(new PacketNodeToManager(name,direction,ID,c.getX(),c.getY(),c.getR(),c.getG(),c.getB())));
					return false;
				}
				return c.getX() < tab.getWidth() - 1 && tab.getElementAtCoords(c.getX() + 1, c.getY()).getType() == MapElement.EMPTY;
		}
	}

	public String getName(){
		return name;
	}
	
}
