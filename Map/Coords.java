package Map;

public class Coords {
	int x,y;
	String ID;
	float r,g,b;
	public Coords(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setID(String ID){
		this.ID = ID;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public String getID(){
		return ID;
	}

	public void setRGB(float r, float g, float b){
		this.r = r;
		this.g = g;
		this.b = b;
	}

	public float getR(){
		return r;
	}

	public float getG(){
		return g;
	}

	public float getB(){
		return b;
	}
}
